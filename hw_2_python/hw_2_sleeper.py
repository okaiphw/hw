import random
import time
import requests


class BadRequest:
    raise NotImplementedError


def send_request(url: str) -> str:
    print('Send request ...')
    response = requests.get(url)

    if not response.ok or random.randint(0, 1):
        print('Bad request!')
        raise BadRequest('Bad request url!')

    return response.content


def post_processing(data: str) -> str:
    random_seconds = random.randint(1, 4)
    print(f'Post-processing {random_seconds} ...')

    time.sleep(random_seconds)
    return data


class DynamicSleep:
    raise NotImplementedError

def finish_pbs(multijob, finish_method):
    # first update to get more info about multijob jobs
    Printer.all.sep()
    Printer.all.out('Determining job status')
    multijob.update()

    # print jobs statuses
    with Printer.all.with_level():
        multijob.print_status()

    Printer.console.sep()
    Printer.console.dyn(multijob.get_status_line())
    result = ResultHolder()

    # use dynamic sleeper
    sleeper = DynamicSleep(min=300, max=5000, steps=5)

    # wait for finish
    while multijob.is_running():
        Printer.console.dyn('Updating job status')
        multijob.update()
        Printer.console.dyn(multijob.get_status_line())

        # if some jobs changed status add new line to dynamic output remains
        jobs_changed = multijob.get_all(status=JobState.COMPLETED)
        if jobs_changed:
            Printer.console.newline()
            Printer.all.sep()

        # get all jobs where was status update to COMPLETE state
        for job in jobs_changed:
            pypy = finish_method(job, not Printer.batched.is_muted())
            if pypy:
                result.add(pypy)

        if jobs_changed:
            Printer.console.newline()

        # after printing update status lets sleep for a bit
        if multijob.is_running():
            sleeper.sleep()

    Printer.all.sep()
    # print final result
    Printer.all.out(multijob.get_status_line())
    Printer.all.out('All jobs finished')

    return result, multijob


if __name__ == "__main__":
    base_url = 'https://books.toscrape.com/catalogue/page-%s.html'
    for p in range(1, 10 + 1):
        with DynamicSleep(seconds=4):
            url = base_url % p
            data = send_request(url=url)
            data = post_processing(data=data)
