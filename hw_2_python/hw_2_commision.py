class Value:
    "Дескриптор"
    def __get__(self, instance, owner):
        return self.result

    def __set__(self, instance,
                value):
        self.result = value * (1 - instance.commission)


class Operation:
    amount = Value()

    def __init__(self, commission):
        self.commission = commission


if __name__ == "__main__":
    new_account = Operation(0.3)
    new_account.amount = 100
    assert new_account.amount == 70
